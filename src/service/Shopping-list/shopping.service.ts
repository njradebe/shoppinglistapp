import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Item } from '../../models/item.model';

@Injectable()
export class shoppingListService {
    
    private shoppingListRef: any = this.db.list<Item>('ShoppingList');

    constructor(private db: AngularFireDatabase){}

    getShoppingList(){

        return this.shoppingListRef;
    }

    addItem(Item: Item){

        return this.shoppingListRef.push(Item);
    }

    editItem(item: Item)
    {
        return this.shoppingListRef.update(item.key, item);
    }

    removeItem(item: Item)
    {
        return this.shoppingListRef.remove(item.key);
    }
}