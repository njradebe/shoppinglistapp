import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { shoppingListService } from '../../service/Shopping-list/shopping.service';
import { Item } from '../../models/item.model';

/**
 * Generated class for the EditShoppingItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-shopping-item',
  templateUrl: 'edit-shopping-item.html',
})
export class EditShoppingItemPage {

  item: Item;

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private shopping: shoppingListService,
     public toastController: ToastController) {
  }

  //toast
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Item Deleted!',
      duration: 2000
    });
    toast.present();
  }

  ionViewWillLoad() {
    this.item = this.navParams.get('item');
  }

  updateItem(item: Item)
  {
    this.shopping.editItem(item)
    .then(() => {
        this.navCtrl.setRoot('HomePage');
    })
  }

  removeItem(items: Item){
    this.shopping.removeItem(items).then(() =>{

      this.presentToast();
        this.navCtrl.setRoot('HomePage');
    });
  }
}
