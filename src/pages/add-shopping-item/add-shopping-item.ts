import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Item } from '../../models/item.model';
import { shoppingListService } from '../../service/Shopping-list/shopping.service';

/**
 * Generated class for the AddShoppingItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-shopping-item',
  templateUrl: 'add-shopping-item.html',
})
export class AddShoppingItemPage {
  
  item: Item = {
    name: '',
    quantity: undefined,
    price: undefined,
  }

  constructor(public navCtrl: NavController,public shopping: shoppingListService, public navParams: NavParams,public alertController: AlertController, public toastController: ToastController) {
  }

  //toast
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Item Added!',
      duration: 2000
    });
    toast.present();
  }

  addItem(g_item: Item)
  {
    this.shopping.addItem(g_item).then(ref=>{
        
      this.presentToast();
        this.navCtrl.setRoot('HomePage', {key: ref.key})
    })  
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AddShoppingItemPage');
  }

}
