import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { shoppingListService } from '../../service/Shopping-list/shopping.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { Item } from '../../models/item.model';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  shoppingList$ : Observable<Item[]>;
  total: number;
  
  constructor(public navCtrl: NavController, public shopping: shoppingListService) {
    console.log("In Home.ts");

    this.shoppingList$ = this.shopping.getShoppingList()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() 
          }))
       )
     );

    this.shoppingList$.subscribe (value =>{
      
      this.total = 0;
      for(let i=0; i < value.length; i++){
        this.total = this.total + parseInt(value[i].price);
      }

      console.log(this.total );
    });

  }

  
}